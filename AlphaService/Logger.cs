﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ASE_ISS
{
    class Logger
    {
        public static string _FilePath { get; set; }
        private static readonly object writelogLock = new object();

        public static void Write(string format, params object[] arg)
        {
            Write(string.Format(format, arg));
        }

        public static void Write(String message)
        {
            lock (writelogLock)
            {
                if (string.IsNullOrEmpty(_FilePath))
                {
                    _FilePath = Directory.GetCurrentDirectory();
                }

                String filename = String.Format("{0}\\log\\{1:yyyyMMdd}_dll.log", _FilePath, DateTime.Now);

                try
                {
                    FileInfo finfo = new FileInfo(filename);
                    if (finfo.Directory.Exists == false)
                    {
                        finfo.Directory.Create();
                    }
                    String writeString = String.Format("{0:HH:mm:ss #} {1}", DateTime.Now, message) + Environment.NewLine;
               
                    File.AppendAllText(filename, writeString, Encoding.Unicode);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Logger write: " + ex.Message);
                }
            }
        }
    }
}
