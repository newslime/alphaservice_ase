﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.IO;

namespace ASE_ISS
{
    internal class Mssql
    {
        private String _conn_str = String.Empty;
        private readonly object sql_lock = new object();

        public Mssql(String server_name, String userid, String password, String catalog)
        {
            _conn_str = String.Format("data source={0}; initial catalog = {1}; user id = {2}; password = {3}", server_name, catalog, userid, password);
        }

        internal String GetClearanceRecordFromSql(DateTime startTime, DateTime endTime, String area, String doorid, String name, String cardnumber, uint accessgranted)
        {
            JArray ja = JArray.Parse(@"[]");
            JObject child;
            SqlCommand cmd;
            lock (sql_lock)
            {
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();

                        cmd = GetSqlCommand(startTime, endTime, area, doorid, name, cardnumber, accessgranted);
                        cmd.Connection = conn;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                child = JObject.Parse("{}");

                                child["RecordID"] = Convert.ToUInt32(reader["RecordID"]);
                                child["EmployeeID"] = reader["EmployeeID"].ToString();
                                child["Name"] = reader["Name"].ToString();
                                child["CardNumber"] = reader["CardNumber"].ToString();
                                child["AccessTime"] = Convert.ToDateTime(reader["AccessTime"]);
                                child["AccessType"] = reader["AccessType"].ToString();
                                child["AccessGranted"] = Convert.ToBoolean(reader["AccessGranted"]);
                                child["DoorID"] = reader["DoorID"].ToString();
                                child["Area"] = reader["Area"].ToString();
                                child["FaceRecognized"] = Convert.ToBoolean(reader["FaceRecognized"]);
                                child["TimeSpent"] = Convert.ToSingle(reader["TimeSpent"]);
                                child["Distance"] = Convert.ToSingle(reader["Distance"]);
                                child["Proba"] = Convert.ToSingle(reader["Proba"]);
                                child["FTPPath"] = reader["FTPPath"].ToString();

                                ja.Insert(0, child);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("GetRecordFromSql: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return ja.ToString();
        }

        internal String GetIllegalListRecordFromSql(DateTime startTime, DateTime endTime, String type, String area)
        {
            JArray ja = JArray.Parse(@"[]");
            JObject node;
            SqlCommand cmd;

            lock (sql_lock)
            {
                //get illegal
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();

                        cmd = GetSqlCommand(startTime, endTime, type, area);
                        cmd.Connection = conn;

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                node = JObject.Parse("{}");
                                node["illegalID"] = Convert.ToInt32(reader["illegalID"]);
                                node["time"] = Convert.ToDateTime(reader["time"]);
                                node["illegalType"] = reader["illegalType"].ToString();
                                node["Area"] = reader["Area"].ToString();
                                node["img"] = Path.GetFileName(reader["FTPPath"].ToString());
                                node["comment"] = reader["comment"].ToString();
                                node["label"] = reader["label"].ToString();

                                ja.Add(node);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("GetIllegalListRecordFromSql: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return ja.ToString();
        }

        internal String GetIllegalDetailFromSql(int illegal_id)
        {
            JObject node = JObject.Parse("{}");
            JObject child;
            SqlCommand cmd;
            JArray faceArray = JArray.Parse(@"[]"); ;

            lock (sql_lock)
            {
                //get illegal
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("Select * from illegalBehavior where illegalID=@id", conn);
                        cmd.Parameters.AddWithValue("@id", illegal_id);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                node["illegalID"] = illegal_id;
                                node["time"] = Convert.ToDateTime(reader["time"]);
                                node["illegalType"] = reader["illegalType"].ToString();
                                node["Area"] = reader["Area"].ToString();
                                node["img"] = Path.GetFileName(reader["FTPPath"].ToString());
                                node["comment"] = reader["comment"].ToString();
                                node["label"] = reader["label"].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("IllegalDetail: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                //get illegalInfo
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("Select * from illegalInfo where illegalID=@id", conn);

                        cmd.Parameters.AddWithValue("@id", illegal_id);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                node["peopleCount"] = Convert.ToInt32(reader["peopleCount"]);
                                node["illegalImg1"] = Path.GetFileName(reader["FTPPath1"].ToString());
                                node["illegalImg2"] = Path.GetFileName(reader["FTPPath2"].ToString());
                                node["illegalImg3"] = Path.GetFileName(reader["FTPPath3"].ToString());
                                node["illegalImg4"] = Path.GetFileName(reader["FTPPath4"].ToString());
                                node["illegalFaceList"] = faceArray;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("IllegalInfoDetail: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                //get illegalFace
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("Select * from suspectInfo where illegalID=@id", conn);

                        cmd.Parameters.AddWithValue("@id", illegal_id);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                child = JObject.Parse("{}");
                                child["illegalFace"] = Path.GetFileName(reader["FTPPath"].ToString());
                                child["illegalEpyID1"] = reader["illegalEpyID1"].ToString();
                                child["illegalProba1"] = reader["illegalProba1"].ToString();
                                child["illegalEpyID2"] = reader["illegalEpyID2"].ToString();
                                child["illegalProba2"] = reader["illegalProba2"].ToString();
                                child["illegalEpyID3"] = reader["illegalEpyID3"].ToString();
                                child["illegalProba3"] = reader["illegalProba3"].ToString();

                                faceArray.Add(child);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("IllegalFaceDetail: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                node["illegalFaceList"] = faceArray;
            }

            //get name and facephoto
            for (int j = 0; j < faceArray.Count; j++)
            {
                child = (JObject)faceArray[j];

                try
                {
                    for (int k = 1; k <= 3; k++)
                    {
                        String key = "illegalEpyID" + k.ToString();
                        String emp_id = child[key].ToString();
                        String emp_name = String.Empty;
                        String emp_img = String.Empty;

                        GetEmployeeNameImage(emp_id, ref emp_name, ref emp_img);

                        key = "illegalEpy" + k.ToString();
                        child[key] = emp_name;

                        key = "illegalImg" + k.ToString();
                        child[key] = emp_img;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write("FacePhotoName: " + ex.Message);
                }
            }

            node["illegalFaceList"] = faceArray;

            Logger.Write(node.ToString());

            return node.ToString();
        }

        internal void InsertBellToSql(String manager_id, String manager_name, String manager_cardnumber, bool access_granted, String area, String doorid, String img)
        {
            lock (sql_lock)
            {
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("Insert into BellLog (ManagerID, ManagerName, ManagerCardNumber, BellTime, AccessTime, AccessGranted, DoorID, Area, FTPPath) Values(@managerid, @managername, @managercardnumber, @belltime, @accesstime, @accessgranted, @doorid, @area, @ftppath) ", conn))
                        {
                            cmd.Parameters.AddWithValue("@managerid", manager_id);
                            cmd.Parameters.AddWithValue("@managername", manager_name);
                            cmd.Parameters.AddWithValue("@managercardnumber", manager_cardnumber);
                            cmd.Parameters.AddWithValue("@belltime", DateTime.Now);
                            cmd.Parameters.AddWithValue("@accesstime", DateTime.Now);
                            cmd.Parameters.AddWithValue("@accessgranted", access_granted);
                            cmd.Parameters.AddWithValue("@doorid", doorid);
                            cmd.Parameters.AddWithValue("@area", area);
                            cmd.Parameters.AddWithValue("@ftppath", img);

                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("InsertBellToSql: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        internal void UpdateIllegalInfoToSql(int illegal_id, String comment, int label)
        {
            lock (sql_lock)
            {
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("Update illegalBehavior set comment = @comment, label = @label where illegalID = @id", conn))
                        {
                            cmd.Parameters.AddWithValue("@comment", comment);
                            cmd.Parameters.AddWithValue("@label", label.ToString());
                            cmd.Parameters.AddWithValue("@id", illegal_id);

                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("UpdateIllegalInfoToSql: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        private SqlCommand GetSqlCommand(DateTime startTime, DateTime endTime, String type, String area)
        {
            String cmd_str = "Select * from illegalBehavior";
            bool iswhere = false, addstatrtime = false, addendtime = false, addtype = false, addarea = false;
            SqlCommand cmd;

            if (startTime != DateTime.MinValue)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }

                cmd_str += "time >= @STARTTIME";
                addstatrtime = true;
            }

            if (endTime != DateTime.MinValue)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "time <= @ENDTIME";
                addendtime = true;
            }

            if (type.CompareTo("") != 0)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "illegalType=@type";
                addtype = true;
            }

            if (area.CompareTo("") != 0)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "Area=@area";
                addarea = true;
            }

            cmd = new SqlCommand(cmd_str);

            if (addstatrtime)
                cmd.Parameters.AddWithValue("@STARTTIME", startTime);

            if (addendtime)
                cmd.Parameters.AddWithValue("@ENDTIME", endTime);

            if (addtype)
                cmd.Parameters.AddWithValue("@type", type);

            if (addarea)
                cmd.Parameters.AddWithValue("@area", area);

            return cmd;
        }

        private SqlCommand GetSqlCommand(DateTime startTime, DateTime endTime, String area, String doorid, String name, String cardnumber, uint accessgranted)
        {
            String cmd_str = "Select * from RecordLog";
            bool iswhere = false, addstatrtime = false, addendtime = false, addarea = false, adddoorid = false, addname = false, addcardnumber = false, addaccessgranted = false;
            SqlCommand cmd;

            if (startTime != DateTime.MinValue)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }

                cmd_str += "AccessTime >= @STARTTIME";
                addstatrtime = true;
            }

            if (endTime != DateTime.MinValue)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "AccessTime <= @ENDTIME";
                addendtime = true;
            }

            if (area.CompareTo("") != 0)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "Area=@area";
                addarea = true;
            }

            if (doorid.CompareTo("") != 0)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "DoorID=@doorid";
                adddoorid = true;
            }

            if (name.CompareTo("") != 0)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "Name like '%' + @name + '%'";

                addname = true;
            }

            if (cardnumber.CompareTo("") != 0)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "CardNumber=@cardnumber";
                addcardnumber = true;
            }

            if (accessgranted < 2)
            {
                if (!iswhere)
                {
                    cmd_str += " where ";
                    iswhere = true;
                }
                else
                {
                    cmd_str += " and ";
                }

                cmd_str += "AccessGranted=@accessgranted";
                addaccessgranted = true;
            }

            cmd = new SqlCommand(cmd_str);

            if (addstatrtime)
                cmd.Parameters.AddWithValue("@STARTTIME", startTime);

            if (addendtime)
                cmd.Parameters.AddWithValue("@ENDTIME", endTime);

            if (addarea)
                cmd.Parameters.AddWithValue("@area", area);

            if (adddoorid)
                cmd.Parameters.AddWithValue("@doorid", doorid);

            if (addname)
                cmd.Parameters.AddWithValue("@name", name);

            if (addcardnumber)
                cmd.Parameters.AddWithValue("@cardnumber", cardnumber);

            if (addaccessgranted)
                cmd.Parameters.AddWithValue("@accessgranted", Convert.ToBoolean(accessgranted));

            return cmd;
        }

        /*internal String GetRecordFromSqlByTime(DateTime startDate, DateTime endDate)
        {
            JArray ja = JArray.Parse(@"[]");
            JObject child;
            lock (sql_lock)
            {
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("Select * from RecordLog where AccessTime >= @STARTDT and AccessTime <= @ENDDT", conn))
                        {
                            cmd.Parameters.AddWithValue("@STARTDT", startDate);
                            cmd.Parameters.AddWithValue("@ENDDT", endDate);

                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    child = JObject.Parse("{}");

                                    child["RecordID"] = Convert.ToUInt32(reader["RecordID"]);
                                    child["EmployeeID"] = reader["EmployeeID"].ToString();
                                    child["Name"] = reader["Name"].ToString();
                                    child["CardNumber"] = reader["CardNumber"].ToString();
                                    child["AccessTime"] = Convert.ToDateTime(reader["AccessTime"]);
                                    child["AccessType"] = reader["AccessType"].ToString();
                                    child["AccessGranted"] = Convert.ToBoolean(reader["AccessGranted"]);
                                    child["DoorID"] = reader["DoorID"].ToString();
                                    child["Area"] = reader["Area"].ToString();
                                    child["FaceRecognized"] = Convert.ToBoolean(reader["FaceRecognized"]);
                                    child["TimeSpent"] = Convert.ToSingle(reader["TimeSpent"]);
                                    child["Distance"] = Convert.ToSingle(reader["Distance"]);
                                    child["Proba"] = Convert.ToSingle(reader["Proba"]);
                                    child["FTPPath"] = reader["FTPPath"].ToString();

                                    ja.Add(child);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("GetRecordFromSqlByTime: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return ja.ToString();
        }*/

        internal void GetEmployeeNameImage(String id, ref String name, ref String emp_img)
        {
            lock (sql_lock)
            {
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("Select * from MemberData where EmployeeID = @id", conn))
                        {
                            cmd.Parameters.AddWithValue("@id", id);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    if (reader["Name"] != null)
                                        name = reader["Name"].ToString();

                                    if (reader["FacePhoto"] != null)
                                    {
                                        try
                                        {
                                            emp_img = Convert.ToBase64String((byte[])(reader["FacePhoto"]));
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Write(id.ToString() + "沒大頭照");
                                        }
                                    }
                                    else
                                    {
                                        Logger.Write(id.ToString() + "沒大頭照");
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("GetEmployeeNameImage: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        internal void AddDoorBell(DateTime belltime, String doorid, String area, String ftppath)
        {
            lock (sql_lock)
            {
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("insert into BellLog values( '', '', '', @belltime, null, 0, @doorid, @area, @ftp. '', '' )", conn))
                        {
                            cmd.Parameters.AddWithValue("@belltime", belltime);
                            cmd.Parameters.AddWithValue("@doorid", doorid);
                            cmd.Parameters.AddWithValue("@area", area);
                            cmd.Parameters.AddWithValue("@ftp", ftppath);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("AddDoorBell: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        internal String GetMemberData(String cardnumber)
        {
            JArray ja = JArray.Parse("[]");
            JObject node;
            DateTime time;
            int quantity;

            lock (sql_lock)
            {
                using (SqlConnection conn = new SqlConnection(_conn_str))
                {
                    try
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("Select * from MemberData where CardNumber = @cardnumber", conn))
                        {
                            cmd.Parameters.AddWithValue("@cardnumber", cardnumber);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    node = JObject.Parse("{}");

                                    node["cardnumber"] = cardnumber;

                                    if (reader["EmployeeID"] != null)
                                        node["EmployeeID"] = reader["EmployeeID"].ToString();

                                    if (reader["UserType"] != null)
                                        node["UserType"] = reader["UserType"].ToString();

                                    if (reader["Name"] != null)
                                        node["Name"] = reader["Name"].ToString();

                                    if (reader["DeptNo"] != null)
                                        node["DeptNo"] = reader["DeptNo"].ToString();

                                    if (reader["DeptName"] != null)
                                        node["DeptName"] = reader["DeptName"].ToString();

                                    if (reader["CreateTime"] != null)
                                    {
                                        DateTime.TryParse(reader["CreateTime"].ToString(), out time);
                                        node["CreateTime"] = time;
                                    }

                                    if (reader["ModifyTime"] != null)
                                    {
                                        DateTime.TryParse(reader["ModifyTime"].ToString(), out time);
                                        node["ModifyTime"] = time;
                                    }

                                    if (reader["FacePboto"] != null)
                                        node["FacePboto"] = Convert.ToBase64String((byte[])(reader["FacePhoto"]));

                                    if (reader["Quantity"] != null)
                                    {
                                        int.TryParse(reader["Quantity"].ToString(), out quantity);
                                        node["Quantity"] = quantity;
                                    }

                                    ja.Add(node);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write("GetMemberData: " + ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            return ja.ToString();
        }
    }
}
