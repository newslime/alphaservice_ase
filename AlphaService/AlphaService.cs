﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Drawing;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;

namespace ASE_ISS
{
    public class AlphaService : IDisposable
    {
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        Mssql _mssql;
        SocketServer _server = null;

        readonly object _record_lock = new object();
        readonly object _bell_lock = new object();
        readonly object _illegal_lock = new object();

        List<JObject> _realtime_record_list = new List<JObject>();
        List<String> _bell_list = new List<String>();
        List<JObject> _illegal_list = new List<JObject>();
        Dictionary<int, JObject> _peopleFacesDic = new Dictionary<int, JObject>();

        public AlphaService(String server_name, String userid, String password, String catalog, String log_path)
        {
            Logger._FilePath = log_path;
            Logger.Write("AlphaService 啟用, sqlname:" + server_name + ", userid:" + userid + ", pwd:" + password + ", catalog:" + catalog);

            _mssql = new Mssql(server_name, userid, password, catalog);

            GetTodayRecord();

            StringBuilder filter;
            String ip = "127.0.0.1";
            String filename = String.Format("{0}\\ipaddress.ini", Logger._FilePath);

            try
            {
                if (_server == null)
                {
                    if (File.Exists(filename))
                    {
                        filter = new StringBuilder(32);
                        GetPrivateProfileString("IPAddress", "ip", "127.0.0.1", filter, 32, filename);

                        ip = filter.ToString();
                    }
                    else
                    {
                        IPHostEntry iphostentry = Dns.GetHostEntry(Dns.GetHostName());
                        foreach (IPAddress ipaddress in iphostentry.AddressList)
                        {
                            if (ipaddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                ip = ipaddress.ToString();
                            }
                        }
                    }

                    IPAddress address;
                    if (IPAddress.TryParse(ip, out address))
                    {
                        switch (address.AddressFamily)
                        {
                            case System.Net.Sockets.AddressFamily.InterNetwork:
                                Logger.Write("IP Address: " + ip);
                                break;

                            case System.Net.Sockets.AddressFamily.InterNetworkV6:
                                Logger.Write("Error IP Address: " + ip);
                                break;

                            default:
                                Logger.Write("Error IP Address: " + ip);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Write("IP Address Check: " + ex.Message);
            }

            _server = new SocketServer(this, ip);
            _server.Open();
        }

        public void Dispose()
        {
            if (_server != null)
            {
                _server.Close();
                _server = null;
            }
        }

        //-----------------------Call Dll Func-----------------------------------
        public String GetRealTimeRecord(ref int max_id)
        {
            JArray ja = JArray.Parse(@"[]");

            lock (_record_lock)
            {
                if (max_id > _realtime_record_list.Count)
                    max_id = 0;

                if (max_id == 0)
                {
                    if (_realtime_record_list.Count > 3)
                    {
                        max_id = _realtime_record_list.Count - 3;
                    }
                }

                for (int i = max_id; i < _realtime_record_list.Count; i++)
                {
                    ja.Add(_realtime_record_list[i]);
                    max_id++;
                }
            }

            return ja.ToString();
        }

        public String GetHistoryRecord(out uint page_totalcount, uint page_count = 30, uint page_index = 0, DateTime startTime = default(DateTime), DateTime endTime = default(DateTime), String area = "", String doorid = "", String name = "", String cardnumber = "", uint accessgranted = 2)
        {
            JArray historyArray = JArray.Parse(_mssql.GetClearanceRecordFromSql(startTime, endTime, area, doorid, name, cardnumber, accessgranted));
            JArray pageOfArray = JArray.Parse("[]");

            page_totalcount = Convert.ToUInt32(historyArray.Count);

            for (int i = 0; i < historyArray.Count; i++)
            {
                if (i >= ((page_index) * page_count)  && i < ((page_index + 1) * page_count))
                {
                    pageOfArray.Add(historyArray[i]);
                }
            }
            
            return pageOfArray.ToString();
        }

        public String GetDoorBell()
        {
            JArray ja = JArray.Parse(@"[]");
            JObject child;
            lock (_bell_lock)
            {
                foreach (String json in _bell_list)
                {
                    child = JObject.Parse(json);
                    ja.Add(child);
                }

                return ja.ToString();
            }
        }

        public bool OpenDoorBell(String json, out String error_msg)
        {
            error_msg = "OK";
            JObject node, child;
            bool access_granted;
            String manager_id, manager_name, manager_cardnumber, doorid, area, imgftp;

            Logger.Write("Web: " + json);

            try
            {
                node = JObject.Parse(json);

                manager_id = node["managerid"].ToString();
                manager_name = node["managername"].ToString();
                manager_cardnumber = node["managercardnumber"].ToString();
                access_granted = Convert.ToBoolean(node["open"]);
                area = node["area"].ToString();
                doorid = node["door"].ToString();
                imgftp = node["img"].ToString();
            }
            catch (Exception ex)
            {
                Logger.Write("AlphaService OpenDoorBell: " + ex.Message);
                error_msg = ex.Message;
                return false;
            }

            if (_server.OpenDoor(json, area, doorid, ref error_msg))
            {
                _mssql.InsertBellToSql(manager_id, manager_name, manager_cardnumber, access_granted, area, doorid, imgftp);
            }
            else
            {
                return false;
            }

            for (int i = 0; i < _bell_list.Count; i++)
            { 
                try
                {
                    child = JObject.Parse(_bell_list[i]);
                    if (area.CompareTo(child["area"].ToString()) == 0 && doorid.CompareTo(child["door"].ToString()) == 0)
                    {
                        _bell_list.RemoveAt(i);
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write("AlphaService OpenDoorBell: " + ex.Message);
                    error_msg = ex.Message;
                    return false;
                }
            }

            return true;
        }

        public String GetIllegalAlarm()
        {
            JArray ja = JArray.Parse(@"[]");

            lock (_illegal_lock)
            {
                try
                {
                    foreach (JObject child in _illegal_list)
                        ja.Insert(0, child);
                }
                catch (Exception ex)
                {
                    Logger.Write("GetIllegalAlarm: " + ex.Message);
                }
            }

            return ja.ToString();
        }

        public bool RemoveIllegalAlarm(int illegal_id, String comment, int label, out String error_msg)
        {
            JObject node = JObject.Parse("{}");
            int id;
            int index = 0;
            bool result = false;
            error_msg = "OK";

#if DEBUG
            _mssql.UpdateIllegalInfoToSql(illegal_id, comment, label);
#else
            lock (_illegal_lock)
            {
                try
                {
                    while (index < _illegal_list.Count)
                    {
                        node = _illegal_list[index];

                        int.TryParse(node["illegalID"].ToString(), out id);

                        if (id == illegal_id)
                        {
                            result = true;
                            _illegal_list.RemoveAt(index);

                            if (_peopleFacesDic.ContainsKey(id))
                                _peopleFacesDic.Remove(id);

                            _mssql.UpdateIllegalInfoToSql(illegal_id, comment, label);
                            PlcControl.WriteCommand("0000");
                            break;
                        }

                        index++;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write("RemoveIllegalAlarm: " + ex.Message);
                }
            }

            if (!result)
                error_msg = "未找到對應的illegalID";
#endif

            return result;
        }

        public String GetIllegalFace(int illegal_id)
        {
            lock (_illegal_lock)
            {
                if (_peopleFacesDic.ContainsKey(illegal_id))
                    return _peopleFacesDic[illegal_id].ToString();

                return "";
            }
        }

        public String GetMemberData(String cardnumber)
        {
            return _mssql.GetMemberData(cardnumber);
        }

        public bool SetIllegalTypeConfig(params bool[] values)
        {
            return _server.SetConfig(values);
        }

        public bool AddIllegalType(String name)
        {
            return _server.AddIllegalType(name);
        }

        public String GetIllegalListRecord(out uint page_totalcount, uint page_count = 30, uint page_index = 0, DateTime startTime = default(DateTime), DateTime endTime = default(DateTime), String type = "", String area = "")
        {
            JArray historyArray = JArray.Parse(_mssql.GetIllegalListRecordFromSql(startTime, endTime, type, area));
            JArray pageOfArray = JArray.Parse("[]");

            page_totalcount = Convert.ToUInt32(historyArray.Count);

            for (int i = 0; i < historyArray.Count; i++)
            {
                if (i >= ((page_index) * page_count) && i < ((page_index + 1) * page_count))
                {
                    pageOfArray.Add(historyArray[i]);
                }
            }

            return pageOfArray.ToString();
        }

        public String GetIllegalDetailRecord(int illegal_id)
        {
            return _mssql.GetIllegalDetailFromSql(illegal_id);
        }

        //--------------------Process Func---------------------------------
        internal void RemainTodayRecord()
        {
            JObject node;
            DateTime datetime;
            int index = 0;

            lock (_record_lock)
            {
                while(index < _realtime_record_list.Count)
                {
                    node = _realtime_record_list[index];

                    DateTime.TryParse(node["AccessTime"].ToString(), out datetime);

                    if (datetime.Date != DateTime.Today)
                    {
                        _realtime_record_list.RemoveAt(index);
                    }
                    else
                    {
                        index++;
                    }
                }
            }
        }

        internal void GetTodayRecord()
        {
            lock (_record_lock)
            {
                String jarray_str = _mssql.GetClearanceRecordFromSql(DateTime.Today, DateTime.Today.AddDays(1), "", "", "", "", 2);
                JArray ja = JArray.Parse(jarray_str);
                JObject node;

                for (int i = 0; i < ja.Count; i++)
                {
                    node = JObject.Parse(ja[i].ToString());
                    _realtime_record_list.Add(node);
                }
            }
        }

        internal void AddRecordList(JObject node)
        {
            lock (_record_lock)
            {
                _realtime_record_list.Add(node);
            }
        }

        internal void AddDoorBell(String json_str)
        {
            lock(_bell_lock)
            {
                _bell_list.Add(json_str);
            }
        }

        internal void AddIllegalAlarm(JObject node)
        {
            lock (_illegal_lock)
            {
                _illegal_list.Add(node);
            }
        }

        internal void AddIllegalFace(String cmd, JObject node)
        {
            int illegal_id, count;
            JObject newnode, child;
            JArray ja;
            String key;
            String emp_name = String.Empty, emp_id = String.Empty, emp_img = String.Empty;

            lock (_illegal_lock)
            {
                try
                {
                    int.TryParse(node["illegalID"].ToString(), out illegal_id);
               
                    switch (cmd)
                    {
                        case "illegalInfo":
                            newnode = JObject.Parse("{}");
                            int.TryParse(node["peopleCount"].ToString(), out count);
                            ja = JArray.Parse(@"[]");

                            newnode["illegalID"] = illegal_id;
                            newnode["peopleCount"] = count;

                            if (node["illegalImg1"] != null)
                                newnode["illegalImg1"] = Path.GetFileName(node["illegalImg1"].ToString());

                            if (node["illegalImg2"] != null)
                                newnode["illegalImg2"] = Path.GetFileName(node["illegalImg2"].ToString());

                            if (node["illegalImg3"] != null)
                                newnode["illegalImg3"] = Path.GetFileName(node["illegalImg3"].ToString());

                            if (node["illegalImg4"] != null)
                                newnode["illegalImg4"] = Path.GetFileName(node["illegalImg4"].ToString());

                            newnode["illegalFaceList"] = ja;
                            _peopleFacesDic[illegal_id] = newnode;
                            break;

                        case "illegalFace":
                            if (_peopleFacesDic.ContainsKey(illegal_id))
                            {
                                newnode = _peopleFacesDic[illegal_id];

                                ja = (JArray)newnode["illegalFaceList"];

                                child = JObject.Parse("{}");
                                child["illegalFace"] = Path.GetFileName(node["illegalFace"].ToString());
                                for (int i = 1; i <= 3; i++)
                                {
                                    emp_name = "";
                                    emp_img = "";
#if DEBUG
                                    key = "illegalEpyID" + i.ToString();
                                    emp_id = node[key].ToString();

                                    child[key] = emp_id;


                                    key = "illegalEpy" + i.ToString();
                                    child[key] = emp_id;

                                    key = "illegalImg" + i.ToString();
                                    child[key] = "null";

                                    key = "illegalProba" + i.ToString();
                                    child[key] = node[key];
#else

                                    key = "illegalEpyID" + i.ToString();
                                    if (node[key] != null)
                                    {
                                        emp_id = node[key].ToString();
                                        child[key] = emp_id;
                                    }

                                    if (emp_id.CompareTo("") != 0)
                                    {
                                        _mssql.GetEmployeeNameImage(emp_id, ref emp_name, ref emp_img);

                                        key = "illegalEpy" + i.ToString();
                                        child[key] = emp_name;

                                        key = "illegalImg" + i.ToString();
                                        child[key] = emp_img;
                                    }

                                    key = "illegalProba" + i.ToString();
                                    if (node[key] != null)
                                    {
                                        child[key] = node[key];
                                    } 
#endif
                                }

                                ja.Add(child);
 
                                newnode["illegalFaceList"] = ja;

                                _peopleFacesDic[illegal_id] = newnode;
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Write("AddIllegalFace: " + ex.Message + "-" + node.ToString());
                }
            }
        }
    }
}
