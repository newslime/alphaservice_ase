﻿namespace ASE_Sample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.realtimeBtn = new System.Windows.Forms.Button();
            this.recordAllBtn = new System.Windows.Forms.Button();
            this.doorbellBtn = new System.Windows.Forms.Button();
            this.opendoorBtn = new System.Windows.Forms.Button();
            this.illegalBtn = new System.Windows.Forms.Button();
            this.removeAlarmBtn = new System.Windows.Forms.Button();
            this.getFaceBtn = new System.Windows.Forms.Button();
            this.illegaltimeBtn = new System.Windows.Forms.Button();
            this.belltimeBtn = new System.Windows.Forms.Button();
            this.plcBtn = new System.Windows.Forms.Button();
            this.illegalRecordBtn = new System.Windows.Forms.Button();
            this.illegalDetailBtn = new System.Windows.Forms.Button();
            this.updateIllegalBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // realtimeBtn
            // 
            this.realtimeBtn.Location = new System.Drawing.Point(26, 26);
            this.realtimeBtn.Margin = new System.Windows.Forms.Padding(4);
            this.realtimeBtn.Name = "realtimeBtn";
            this.realtimeBtn.Size = new System.Drawing.Size(200, 33);
            this.realtimeBtn.TabIndex = 0;
            this.realtimeBtn.Text = "GetRealTimeRecord";
            this.realtimeBtn.UseVisualStyleBackColor = true;
            this.realtimeBtn.Click += new System.EventHandler(this.realtimeBtn_Click);
            // 
            // recordAllBtn
            // 
            this.recordAllBtn.Location = new System.Drawing.Point(26, 66);
            this.recordAllBtn.Name = "recordAllBtn";
            this.recordAllBtn.Size = new System.Drawing.Size(200, 30);
            this.recordAllBtn.TabIndex = 1;
            this.recordAllBtn.Text = "GetHistoryRecord";
            this.recordAllBtn.UseVisualStyleBackColor = true;
            this.recordAllBtn.Click += new System.EventHandler(this.recordAllBtn_Click);
            // 
            // doorbellBtn
            // 
            this.doorbellBtn.Location = new System.Drawing.Point(26, 164);
            this.doorbellBtn.Name = "doorbellBtn";
            this.doorbellBtn.Size = new System.Drawing.Size(200, 30);
            this.doorbellBtn.TabIndex = 3;
            this.doorbellBtn.Text = "GetDoorBell";
            this.doorbellBtn.UseVisualStyleBackColor = true;
            this.doorbellBtn.Click += new System.EventHandler(this.doorbellBtn_Click);
            // 
            // opendoorBtn
            // 
            this.opendoorBtn.Location = new System.Drawing.Point(26, 200);
            this.opendoorBtn.Name = "opendoorBtn";
            this.opendoorBtn.Size = new System.Drawing.Size(200, 30);
            this.opendoorBtn.TabIndex = 4;
            this.opendoorBtn.Text = "OpenDoor";
            this.opendoorBtn.UseVisualStyleBackColor = true;
            this.opendoorBtn.Click += new System.EventHandler(this.opendoorBtn_Click);
            // 
            // illegalBtn
            // 
            this.illegalBtn.Location = new System.Drawing.Point(26, 292);
            this.illegalBtn.Name = "illegalBtn";
            this.illegalBtn.Size = new System.Drawing.Size(200, 30);
            this.illegalBtn.TabIndex = 5;
            this.illegalBtn.Text = "GetIllegalAlarm";
            this.illegalBtn.UseVisualStyleBackColor = true;
            this.illegalBtn.Click += new System.EventHandler(this.illegalBtn_Click);
            // 
            // removeAlarmBtn
            // 
            this.removeAlarmBtn.Location = new System.Drawing.Point(26, 328);
            this.removeAlarmBtn.Name = "removeAlarmBtn";
            this.removeAlarmBtn.Size = new System.Drawing.Size(200, 35);
            this.removeAlarmBtn.TabIndex = 6;
            this.removeAlarmBtn.Text = "RemoveIllegalAlarm";
            this.removeAlarmBtn.UseVisualStyleBackColor = true;
            this.removeAlarmBtn.Click += new System.EventHandler(this.removeAlarmBtn_Click);
            // 
            // getFaceBtn
            // 
            this.getFaceBtn.Location = new System.Drawing.Point(26, 369);
            this.getFaceBtn.Name = "getFaceBtn";
            this.getFaceBtn.Size = new System.Drawing.Size(200, 30);
            this.getFaceBtn.TabIndex = 7;
            this.getFaceBtn.Text = "GetIllegalFace";
            this.getFaceBtn.UseVisualStyleBackColor = true;
            this.getFaceBtn.Click += new System.EventHandler(this.getFaceBtn_Click);
            // 
            // illegaltimeBtn
            // 
            this.illegaltimeBtn.Location = new System.Drawing.Point(26, 405);
            this.illegaltimeBtn.Name = "illegaltimeBtn";
            this.illegaltimeBtn.Size = new System.Drawing.Size(200, 35);
            this.illegaltimeBtn.TabIndex = 8;
            this.illegaltimeBtn.Text = "SetIllegalTime";
            this.illegaltimeBtn.UseVisualStyleBackColor = true;
            this.illegaltimeBtn.Click += new System.EventHandler(this.illegaltimeBtn_Click);
            // 
            // belltimeBtn
            // 
            this.belltimeBtn.Location = new System.Drawing.Point(26, 236);
            this.belltimeBtn.Name = "belltimeBtn";
            this.belltimeBtn.Size = new System.Drawing.Size(200, 30);
            this.belltimeBtn.TabIndex = 10;
            this.belltimeBtn.Text = "SetBellTime";
            this.belltimeBtn.UseVisualStyleBackColor = true;
            this.belltimeBtn.Click += new System.EventHandler(this.belltimeBtn_Click);
            // 
            // plcBtn
            // 
            this.plcBtn.Location = new System.Drawing.Point(259, 29);
            this.plcBtn.Name = "plcBtn";
            this.plcBtn.Size = new System.Drawing.Size(131, 30);
            this.plcBtn.TabIndex = 11;
            this.plcBtn.Text = "PLC Control";
            this.plcBtn.UseVisualStyleBackColor = true;
            this.plcBtn.Click += new System.EventHandler(this.plcBtn_Click);
            // 
            // illegalRecordBtn
            // 
            this.illegalRecordBtn.Location = new System.Drawing.Point(259, 162);
            this.illegalRecordBtn.Name = "illegalRecordBtn";
            this.illegalRecordBtn.Size = new System.Drawing.Size(200, 35);
            this.illegalRecordBtn.TabIndex = 12;
            this.illegalRecordBtn.Text = "GetIllegalRecord";
            this.illegalRecordBtn.UseVisualStyleBackColor = true;
            this.illegalRecordBtn.Click += new System.EventHandler(this.illegalRecordBtn_Click);
            // 
            // illegalDetailBtn
            // 
            this.illegalDetailBtn.Location = new System.Drawing.Point(259, 203);
            this.illegalDetailBtn.Name = "illegalDetailBtn";
            this.illegalDetailBtn.Size = new System.Drawing.Size(200, 35);
            this.illegalDetailBtn.TabIndex = 13;
            this.illegalDetailBtn.Text = "GetIllegalDetail";
            this.illegalDetailBtn.UseVisualStyleBackColor = true;
            this.illegalDetailBtn.Click += new System.EventHandler(this.illegalDetailBtn_Click);
            // 
            // updateIllegalBtn
            // 
            this.updateIllegalBtn.Location = new System.Drawing.Point(259, 292);
            this.updateIllegalBtn.Name = "updateIllegalBtn";
            this.updateIllegalBtn.Size = new System.Drawing.Size(200, 35);
            this.updateIllegalBtn.TabIndex = 14;
            this.updateIllegalBtn.Text = "updateIllegal";
            this.updateIllegalBtn.UseVisualStyleBackColor = true;
            this.updateIllegalBtn.Click += new System.EventHandler(this.updateIllegalBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 572);
            this.Controls.Add(this.updateIllegalBtn);
            this.Controls.Add(this.illegalDetailBtn);
            this.Controls.Add(this.illegalRecordBtn);
            this.Controls.Add(this.plcBtn);
            this.Controls.Add(this.belltimeBtn);
            this.Controls.Add(this.illegaltimeBtn);
            this.Controls.Add(this.getFaceBtn);
            this.Controls.Add(this.removeAlarmBtn);
            this.Controls.Add(this.illegalBtn);
            this.Controls.Add(this.opendoorBtn);
            this.Controls.Add(this.doorbellBtn);
            this.Controls.Add(this.recordAllBtn);
            this.Controls.Add(this.realtimeBtn);
            this.Font = new System.Drawing.Font("Microsoft JhengHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "ASE Sample";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button realtimeBtn;
        private System.Windows.Forms.Button recordAllBtn;
        private System.Windows.Forms.Button doorbellBtn;
        private System.Windows.Forms.Button opendoorBtn;
        private System.Windows.Forms.Button illegalBtn;
        private System.Windows.Forms.Button removeAlarmBtn;
        private System.Windows.Forms.Button getFaceBtn;
        private System.Windows.Forms.Button illegaltimeBtn;
        private System.Windows.Forms.Button belltimeBtn;
        private System.Windows.Forms.Button plcBtn;
        private System.Windows.Forms.Button illegalRecordBtn;
        private System.Windows.Forms.Button illegalDetailBtn;
        private System.Windows.Forms.Button updateIllegalBtn;
    }
}

