﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using ASE_ISS;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.Threading;
using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;

namespace ASE_Sample
{
    public partial class Form1 : Form
    {
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        AlphaService testDll = null;
        int maxid = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
#if DEBUG
            //昱冠內部測試
            testDll = new AlphaService("210.71.208.234", "iss", "AlphaInf<<<qazxsw", "FaceDB", "D:\\test");
#else
            testDll = new AlphaService("210.71.208.234", "iss", "AlphaInf<<<qazxsw", "FaceDB", "D:\\test");
#endif
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (testDll != null)
            {
                testDll.Dispose();
                testDll = null;
            }
        }

        private void realtimeBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 讀取最新人臉訊息
            /// </summary>
            /// <param name="maxid">紀錄最新的record</param>
            /// <return name="json">
            /// {
            ///     "AccessGranted": true, 
            ///     "AccessTime": 2018-12-12T06:28:49, 
            ///     "AccessType":"\u5168\u4eba\u81c9", 
            ///     "Area": "Alpha1", 
            ///     "CardNumber": "25000107", 
            ///     "Distance": 0.172, 
            ///     "DoorID": "Front1", 
            ///     "EmployeeID": "103", 
            ///     "FTPPath": "/home/alpha/resultData/20181207/Alpha1_Front1_20181207_145248.823.jpg", 
            ///     "FaceRecognized": true, 
            ///     "Name": "高富媚", 
            ///     "Proba": "0.9948260188102722"
            ///     }
            ///     <return>
     
            String json = testDll.GetRealTimeRecord(ref maxid);

            MessageBox.Show(json);
        }

        private void recordAllBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 讀取歷史紀錄(總頁數)
            /// <param name="start">開始時間</param>
            /// <param name="end">結束時間</param>
            /// <param name="area">廠區</param>
            /// <param name="doorid">門號</param>
            /// <param name="name">名字</param>
            /// <param name="cardnumber">卡號</param>
            /// <param name="accessgranted">通關結果</param>
            /// </summary>

            uint total_count;
            DateTime start = new DateTime(2018, 2, 1, 0, 0, 0);
            DateTime end = new DateTime(2019, 2, 1, 0, 0, 0);
            String json = testDll.GetHistoryRecord(out total_count, 30, 3, start, end, "", "", "", "", 2);

            JArray array = JArray.Parse(json);

            MessageBox.Show(json);
            Console.WriteLine(json);
        }

        private void doorbellBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 有人按取門鈴，讀取
            /// </summary>
            /// <return name="json">
            /// {
            ///     "cmd": "bell",img
            ///     "area": "ch1",
            ///     "door": "c053456",
            ///     "": "/home/alpha/XXX/XXX.jpg"
            ///     }
            ///     <return>
            ///     
            String json = testDll.GetDoorBell();

            MessageBox.Show(json);
        }

        private void opendoorBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 回應門鈴開門
            /// </summary>
            /// <param name="json">開門json封包
            /// {
            ///     "managerid": "147258",
            ///     "managername": "Slime",
            ///     "managercardnumber": "12345789",
            ///     "area": "ch1",
            ///     "door": "c053456",
            ///     "open": true,
            ///     "img": "/home/alpha/XXX/XXX.jpg"
            ///     }
            ///</param>

            String msg;

            JObject node = JObject.Parse("{}");
            node["managerid"] = "147258";
            node["managername"] = "Slime";
            node["managercardnumber"] = "12345789";
            node["area"] = "ch1";
            node["door"] = "c053456";
            node["open"] = true;
            node["img"] = "/img/xxx.jpg";
            testDll.OpenDoorBell(node.ToString(), out msg);
        }

        private void belltimeBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 設定門鈴保存時間(超過時間自動移除)
            /// </summary>
            /// <param name="seconds">
            ///</param>
            ///
            //testDll.SetDoorBellTime(30);
        }

        private void illegalBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 違規資訊
            /// </summary>
            /// <return name="json">
            /// [
            ///     {   
            ///         "illegalID": "00001",
            ///         "time": 2018-12-12T06:28:49,
            ///         "illegalType": "othercard",
            ///         "location": "K22N",
            ///	        "img":"/home/alpha/XXX/XXX.jpg"
            ///	     }
            ///	     {   
            ///         "illegalID": "00002",
            ///         "time": 2018-12-14T06:28:49,
            ///         "illegalType": "othercard",
            ///         "location": "K23N",
            ///	        "img":"/home/alpha/XXX/XXX.jpg"
            ///	     }
            ///  ]
            ///     <return>
            ///     

            String json = testDll.GetIllegalAlarm();

            MessageBox.Show(json);
        }

        private void removeAlarmBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 移除違規
            /// </summary>
            /// <param name="illegalID">
            ///</param>
            ///
            String msg;

            //testDll.RemoveIllegalAlarm(40001, out msg);
        }

        private void getFaceBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 違規資訊(可能同時有多組違規)
            /// </summary>
            /// <param name="illegalID">
            /// </param>
            /// <return name="json">
            /// {   
            ///    "peopleCount": "2",
            ///    "illegalImg1": "/home/alpha/XXX/XXX.jpg",
            ///    "illegalImg2": "/home/alpha/XXX/XXX2.jpg",
            ///    "illegalFace": {
            ///                         "illegalFace": "/home/alpha/XXX/XXX3.jpg",
            ///                         "illegalEpyID1": "111111",
            ///                         "illegalProba1": "0.85",
            ///                         "illegalEpy1": "王大明",
            ///                         "illegalImg1": byte array,
            ///                         "illegalEpyID2": "222222",
            ///                         "illegalProba2": "0.10",
            ///                         "illegalEpy2": "王大明2",
            ///                         "illegalImg2": byte array,
            ///                         "illegalEpyID3": "333333",
            ///                         "illegalProba3": "0.04",
            ///                         "illegalEpy3": "王大明3",
            ///                         "illegalImg3": byte array
            ///                    }
            ///    "illegalFace2": {
            ///                         "illegalFace": "/home/alpha/XXX/XXX4.jpg",
            ///                         "illegalEpyID1": "111111",
            ///                         "illegalProba1": "0.85",
            ///                         "illegalEpy1": "李四1",
            ///                         "illegalImg1": byte array,
            ///                         "illegalEpyID2": "222222",
            ///                         "illegalProba2": "0.10",
            ///                         "illegalEpy2": "李四2",
            ///                         "illegalImg2": byte array,
            ///                         "illegalEpyID3": "333333",
            ///                         "illegalProba3": "0.04",
            ///                         "illegalEpy3": 李四3",
            ///                         "illegalImg3": byte array
            ///                    }
            /// }
            ///     <return>
            ///     

            String json = testDll.GetIllegalFace(40001);

            MessageBox.Show(json);
        }

        private void illegaltimeBtn_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// 設定違規保存時間(超過時間自動移除)
            /// </summary>
            /// <param name="seconds">
            ///</param>
            ///
            //testDll.SetIllegalAppearTime(30);
        }

        private void plcBtn_Click(object sender, EventArgs e)
        {
        }

        private void illegalRecordBtn_Click(object sender, EventArgs e)
        {
            uint total_count;
            DateTime start = new DateTime(2019, 3, 11, 0, 0, 0);
            DateTime end = new DateTime(2019, 3, 11, 23, 0, 0);
            String json = testDll.GetIllegalListRecord(out total_count, 30, 0, DateTime.MinValue, DateTime.MinValue, "OverSide", "");

            MessageBox.Show(json);
        }

        private void illegalDetailBtn_Click(object sender, EventArgs e)
        {
            String json = testDll.GetIllegalDetailRecord(51380);

            MessageBox.Show(json);
        }

        private void updateIllegalBtn_Click(object sender, EventArgs e)
        {
            /*String message;
            testDll.RemoveIllegalAlarm(51382, "打掃阿姨", -1, out message);

            MessageBox.Show(message);*/

            testDll.AddIllegalType("newslime");
        }
    } 
}
